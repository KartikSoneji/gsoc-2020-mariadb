Hi,

== This week
* Started working on changing the build system to make the plugins call our functions instead of the library functions as per step 4 of https://jira.mariadb.org/browse/MDEV-22895[MDEV-22895].

* I faced a lot of issues with `cmake`, as `INCLUDE_DIRECTORIES` does not work with `CHECK_INCLUDE_FILES`, and setting `CMAKE_REQUIRED_INCLUDES` in `cmake/plugins.cmake` did not affect `storage/innobase/lzma.cmake`. +
  The only thing that worked was `STRING(APPEND CMAKE_C_FLAGS " -I${CMAKE_SOURCE_DIR}/include/mysql")`, but that sets the flag globally, so is not ideal.

* I spoke with some devs on the CMake IRC (`#cmake`), and it seems that InnoDB uses improper CMake commands. It might be easier for the project to rewrite them instead of trying to work around the issues.


== Next week
* Review the CMake files and finish updating the build system as per current standards.
* Continue work on step 5 of https://jira.mariadb.org/browse/MDEV-22895[MDEV-22895].

Thanks, +
Kartik
