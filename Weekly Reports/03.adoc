Hi,

== This week
* I Worked on https://jira.mariadb.org/browse/MDEV-22895[MDEV-22895].

* Added the `--use-compression=` switch to the server. +
  https://github.com/KartikSoneji/server/tree/feature-add-compression-library-switch

* Started work on service for the ZLib library in https://github.com/KartikSoneji/server/tree/feature-compression-service-zlib[feature-compression-zlib].

* Worked on service for the LZMA library in https://github.com/KartikSoneji/server/tree/feature-compression-service-lzma[feature-compression-lzma].

* Design consideration and documented the implications and tradeoffs of using services instead of a Unified API.


== Next week
* I plan to finish writing the service for LZMA.
* Test out the service.

Thanks, +
Kartik
