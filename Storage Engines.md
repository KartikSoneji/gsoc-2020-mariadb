## Storage Engines
- Archive
- Aria
- Blackhole
- Cassandra
- Connect
- Csv
- Federated
- Federatedx
- Heap
- InnoDB
- Mroonga
- Myisam
- Myisammrg
- Oqgraph
- Perfschema
- RocksDB
- Sequence
- Sphinx
- Spider
- ~~Tokudb~~ (deprecated)

<table>
<thead>
	<tr>
		<th>Storage Engine</th>
		<th>Links</th>
		<th>Compression Libraries</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>Archive</td>
		<td>
			<a href = "https://dev.mysql.com/doc/refman/5.7/en/archive-storage-engine.html">
				&#x1F517;
			</a>
		</td>
		<td>AZlib</td>
	</tr>
	<tr>
		<td>Connect</td>
		<td></td>
		<td>BZip2, Zlib</td>
	</tr>
	<tr>
		<td>InnoDB</td>
		<td>
			<a href = "https://mariadb.com/kb/en/innodb-page-compression/#configuring-the-innodb-page-compression-algorithm">
				&#x1F517;
			</a>
		</td>
		<td>BZip2, LZ4, LZMA, LZO, Snappy, Zlib</td>
	</tr>
	<tr>
		<td>Aria</td>
		<td></td>
		<td>Zlib</td>
	</tr>
	<tr>
		<td>Mroonga</td>
		<td>
			<a href = "https://mroonga.org/docs/reference/server_variables.html#mroonga-libgroonga-support-lz4">
				&#x1F517;
			</a>
		</td>
		<td>LZ4, Zlib, ZStandard</td>
	</tr>
	<tr>
		<td>RocksDB</td>
		<td>
			<a href = "https://mariadb.com/kb/en/myrocks-and-data-compression/#supported-compression-algorithms">
				&#x1F517;
			</a>
		</td>
		<td>BZip2, LZ4, LZ4HC, Snappy, Xpress, Zlib, ZStandard</td>
	</tr>
	<tr>
		<td><s>Tokudb</s> (depricated)</td>
		<td></td>
		<td><s>FastLZ, LZ, LZF, LZMA, LZMA2, LZO, QuickLZ, Snappy, Zlib</s></td>
	</tr>
</tbody>
</table>


\       |AZlib|BZip2|LZ4|LZ4HC|LZMA|LZO|Snappy|Xpress|Zlib|ZStandard
--------|-----|-----|---|-----|----|---|------|------|----|---------
Archive |Y    |     |   |     |    |   |      |      |    |         
Connect |     |Y    |   |     |    |   |      |      |Y   |         
InnoDB  |     |Y    |Y  |     |Y   |Y  |Y     |      |Y   |         
Aria    |     |     |   |     |    |   |      |      |Y   |         
Mroonga |     |     |Y  |     |    |   |      |      |Y   |Y        
RocksDB |     |Y    |Y  |Y    |    |   |Y     |Y     |Y   |Y        
20      |1    |3    |2  |1    |1   |1  |2     |1     |5   |2        


Library  |License
---------|-------
AZlib    |[Zlib](https://zlib.net/zlib_license.html)
BZip2    |[BSD-like](http://www.bzip.org/#:~:text=BSD-like)
LZ4      |[Library files under BSD 2-Clause and other under GPL v2](https://github.com/lz4/lz4/blob/dev/LICENSE)
LZMA     |[Public Domain](https://www.7-zip.org/sdk.html#:~:text=License-,LZMA,domain)
LZO      |[GPL v2+](http://www.oberhumer.com/opensource/lzo/#:~:text=GPL%20v2+)
Snappy   |[Custom](https://github.com/google/snappy/blob/master/COPYING)
Xpress   |[???](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-xca/a8b7cb0a-92a6-4187-a23b-5e14273b96f8)
Zlib     |[Zlib](https://zlib.net/zlib_license.html)
ZStandard|[Dual: BSD + GPL v2](https://github.com/facebook/zstd#:~:text=dual,GPLv2)

Library  |`apt install`|`gcc --print-file-name=`|`/usr/include`       
---------|-------------|------------------------|--------------
AZlib    |             |                        |
BZip2    |libbz2-dev   |libbz2.so               |bzlib.h
LZ4      |liblz4-dev   |liblz4.so               |lz4.h, lz4frame.h, lz4frame_static.h
LZ4HC    |liblz4-dev   |liblz4.so               |lz4hc.h
LZMA     |liblzma-dev  |liblzma.so              |lzma.h, lzma/*
LZO      |liblzo2-dev  |liblzo2.so              |lzo/*
Snappy   |libsnappy-dev|libsnappy.so            |snappy-c.h, snappy-sinksource.h, snappy-stubs-public.h, snappy.h
Xpress   |             |                        |
Zlib     |libz-dev     |libz.so                 |zlib.h
ZStandard|libzstd-dev  |libzstd.so              |zstd.h, zstd_errors.h

Install all libraries:  
`sudo apt install -y libbz2-dev liblz4-dev liblzma-dev liblzo2-dev libsnappy-dev libz-dev libzstd-dev`.

## Header files in use
```
bzlib.h
lz4.h
lz4hc.h
lzma.h
lzo/lzo1x.h
snappy-c.h
snappy.h
zlib.h
zstd.h
```

Xpress is Windows only:  
https://github.com/facebook/rocksdb/blob/master/port/xpress.h
```C++
#pragma once

// Xpress on Windows is implemeted using Win API
#if defined(ROCKSDB_PLATFORM_POSIX)
    #error "Xpress compression not implemented"
#elif defined(OS_WIN)
    #include "port/win/xpress_win.h"
#endif
```

And has 2 functions:  
https://github.com/facebook/rocksdb/blob/master/port/win/xpress_win.h
```C++
#pragma once
#include <string>
#include "rocksdb/rocksdb_namespace.h"

namespace ROCKSDB_NAMESPACE {
    namespace port {
        namespace xpress {
            bool Compress(const char* input, size_t length, std::string* output);
            char* Decompress(const char* input_data, size_t input_length, int* decompress_size);
        }
    }
}  // namespace ROCKSDB_NAMESPACE
```


## Functions

### Archive
#### AZlib
```
crc32
deflate
deflateEnd
gzclose
gzerror
gzflush
gzread
gzrewind
gzseek
gztell
gzwrite
inflate
inflateEnd
inflateReset
uncompress
```



### Connect
#### BZip2
```
BZ2_bzCompress
BZ2_bzCompressEnd
BZ2_bzCompressInit
BZ2_bzDecompress
BZ2_bzDecompressEnd
BZ2_bzDecompressInit
```

#### Zlib
```
crc32
deflate
deflateEnd
get_crc_table
gzclose
gzeof
gzerror
gzflush
gzgets
gzopen
gzputs
gzread
gzrewind
gzseek
gztell
gzwrite
inflate
inflateEnd
```



### InnoDB
#### BZip2
```
BZ2_bzBuffToBuffCompress
BZ2_bzBuffToBuffDecompress
```

#### LZ4
```
LZ4_compress
LZ4_compress_default
LZ4_compress_limitedOutput
LZ4_decompress_safe
```

#### LZMA
```
lzma_easy_buffer_encode
lzma_stream_buffer_decode
```

#### LZO
```
lzo1x_1_15_compress
lzo1x_decompress_safe
```

#### Snappy
```
snappy_compress
snappy_max_compressed_length
snappy_uncompress
```

#### Zlib
```
adler32
compress2
compressBound
deflate
deflateEnd
deflateReset
inflate
inflateEnd
inflateInit
inflateInit2
uncompress
```



### Aria
#### Zlib
```
compress
crc32
uncompress
```



### Mroonga
#### LZ4
```
LZ4_compress
LZ4_compressBound
LZ4_compress_default
LZ4_decompress_safe
```

#### Zlib
```
compress
compressBound
deflate
deflateBound
deflateEnd
deflateInit2
inflate
inflateEnd
inflateInit2
inflateReset
uncompress
```

#### ZStandard
```
ZSTD_compress
ZSTD_compressBound
ZSTD_decompress
ZSTD_getErrorName
ZSTD_isError
```




### RocksDB
#### BZip2
```
BZ2_bzCompress
BZ2_bzCompressEnd
BZ2_bzCompressInit
BZ2_bzDecompress
BZ2_bzDecompressEnd
BZ2_bzDecompressInit
```

#### LZ4
```
LZ4_compressBound
LZ4_compress_fast_continue
LZ4_compress_limitedOutput
LZ4_compress_limitedOutput_continue
LZ4_createStream
LZ4_createStreamDecode
LZ4_decompress_safe
LZ4_decompress_safe_continue
LZ4_freeStream
LZ4_freeStreamDecode
LZ4_loadDict
```

#### LZ4HC
```
LZ4_compressHC2_limitedOutput
LZ4_compressHC_limitedOutput
LZ4_compressHC_limitedOutput_continue
LZ4_compress_HC_continue
LZ4_createStreamHC
LZ4_freeStreamHC
LZ4_loadDictHC
LZ4_resetStreamHC
```

#### Snappy
```
snappy::GetUncompressedLength
snappy::MaxCompressedLength
snappy::RawCompress
snappy::RawUncompress
```

#### Xpress* (Windows only)
```
Compress
Decompress
```

#### Zlib
```
crc32
deflate
deflateEnd
deflateSetDictionary
inflate
inflateEnd
inflateSetDictionary
```

#### ZStandard
```
Undefined ZSTD_VERSION_NUMBER

ZSTD_compress
ZSTD_compressBound
ZSTD_decompress
ZSTD_versionNumber

With ZSTD_VERSION_NUMBER >= 800

ZSTD_compressBound
ZSTD_compress_usingCDict
ZSTD_compress_usingDict
ZSTD_createCCtx
ZSTD_createCDict
ZSTD_createDCtx
ZSTD_decompress_usingDict
ZSTD_freeCCtx
ZSTD_freeCDict
ZSTD_freeDCtx
ZSTD_versionNumber

With ROCKSDB_ZSTD_CUSTOM_MEM
ZSTD_createCCtx_advanced
ZSTD_createDCtx_advanced

With ROCKSDB_ZSTD_DDICT
ZSTD_createDDict_byReference
ZSTD_decompress_usingDDict
ZSTD_freeDDict
ZSTD_sizeof_DDict
```