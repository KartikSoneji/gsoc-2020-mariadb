From b48cbdcc67bbcb0959b93006348d68db96e0cbf1 Mon Sep 17 00:00:00 2001
From: Kartik Soneji <kartiksoneji@rocketmail.com>
Date: Sat, 29 Aug 2020 05:44:00 +0530
Subject: [PATCH 06/15] Create service for ZStd.

---
 include/compression/zstd.h                    | 77 +++++++++++++++++++
 include/mysql/plugin_audit.h.pp               | 16 ++++
 include/mysql/plugin_auth.h.pp                | 16 ++++
 include/mysql/plugin_data_type.h.pp           | 16 ++++
 include/mysql/plugin_encryption.h.pp          | 16 ++++
 include/mysql/plugin_ftparser.h.pp            | 16 ++++
 include/mysql/plugin_function.h.pp            | 16 ++++
 include/mysql/plugin_password_validation.h.pp | 16 ++++
 include/mysql/services.h                      |  1 +
 include/service_versions.h                    |  1 +
 libmysqld/CMakeLists.txt                      |  1 +
 libservices/CMakeLists.txt                    |  1 +
 libservices/compression_service_zstd.c        | 14 ++++
 sql/CMakeLists.txt                            |  1 +
 sql/compression/compression_libs.cc           |  4 +-
 sql/compression/compression_libs.h            |  5 +-
 sql/compression/zstd.cc                       | 71 +++++++++++++++++
 sql/mysqld.cc                                 |  1 +
 sql/sql_plugin.cc                             |  3 +-
 sql/sql_plugin_services.ic                    |  4 +-
 20 files changed, 292 insertions(+), 4 deletions(-)
 create mode 100644 include/compression/zstd.h
 create mode 100644 libservices/compression_service_zstd.c
 create mode 100644 sql/compression/zstd.cc

diff --git a/include/compression/zstd.h b/include/compression/zstd.h
new file mode 100644
index 00000000..68c6f626
--- /dev/null
+++ b/include/compression/zstd.h
@@ -0,0 +1,77 @@
+/**
+  @file include/compression/zstd.h
+  This service provides dynamic access to ZStandard.
+*/
+
+#ifndef SERVICE_ZSTD_INCLUDED
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+#ifndef MYSQL_ABI_CHECK
+#include <stdbool.h>
+#include <stddef.h>
+#endif
+
+extern bool COMPRESSION_LOADED_ZSTD;
+
+#define DEFINE_ZSTD_compress(NAME) size_t NAME( \
+    void *dst,                                  \
+    size_t dstCapacity,                         \
+    const void *src,                            \
+    size_t srcSize,                             \
+    int compressionLevel                        \
+)
+
+#define DEFINE_ZSTD_compressBound(NAME) size_t NAME(    \
+    size_t srcSize                                      \
+)
+
+#define DEFINE_ZSTD_decompress(NAME) size_t NAME(   \
+    void *dst,                                      \
+    size_t dstCapacity,                             \
+    const void *src,                                \
+    size_t compressedSize                           \
+)
+
+#define DEFINE_ZSTD_getErrorName(NAME) const char* NAME(    \
+    size_t code                                             \
+)
+
+#define DEFINE_ZSTD_isError(NAME) unsigned NAME(    \
+    size_t code                                     \
+)
+
+
+typedef DEFINE_ZSTD_compress      ((*PTR_ZSTD_compress));
+typedef DEFINE_ZSTD_compressBound ((*PTR_ZSTD_compressBound));
+typedef DEFINE_ZSTD_decompress    ((*PTR_ZSTD_decompress));
+typedef DEFINE_ZSTD_getErrorName  ((*PTR_ZSTD_getErrorName));
+typedef DEFINE_ZSTD_isError       ((*PTR_ZSTD_isError));
+
+
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress      ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress    ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName  ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError       ZSTD_isError_ptr;
+};
+
+
+extern struct compression_service_zstd_st *compression_service_zstd;
+
+
+#define ZSTD_compress(...)      compression_service_zstd->ZSTD_compress_ptr      (__VA_ARGS__)
+#define ZSTD_compressBound(...) compression_service_zstd->ZSTD_compressBound_ptr (__VA_ARGS__)
+#define ZSTD_decompress(...)    compression_service_zstd->ZSTD_decompress_ptr    (__VA_ARGS__)
+#define ZSTD_getErrorName(...)  compression_service_zstd->ZSTD_getErrorName_ptr  (__VA_ARGS__)
+#define ZSTD_isError(...)       compression_service_zstd->ZSTD_isError_ptr       (__VA_ARGS__)
+
+
+#ifdef __cplusplus
+}
+#endif
+
+#define SERVICE_ZSTD_INCLUDED
+#endif
\ No newline at end of file
diff --git a/include/mysql/plugin_audit.h.pp b/include/mysql/plugin_audit.h.pp
index c4bc2d7d..361f767a 100644
--- a/include/mysql/plugin_audit.h.pp
+++ b/include/mysql/plugin_audit.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_auth.h.pp b/include/mysql/plugin_auth.h.pp
index 9d77c8a9..59edb769 100644
--- a/include/mysql/plugin_auth.h.pp
+++ b/include/mysql/plugin_auth.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_data_type.h.pp b/include/mysql/plugin_data_type.h.pp
index 76f0d9a3..c52c52c7 100644
--- a/include/mysql/plugin_data_type.h.pp
+++ b/include/mysql/plugin_data_type.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_encryption.h.pp b/include/mysql/plugin_encryption.h.pp
index 17480c91..a9a67761 100644
--- a/include/mysql/plugin_encryption.h.pp
+++ b/include/mysql/plugin_encryption.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_ftparser.h.pp b/include/mysql/plugin_ftparser.h.pp
index 48d29f47..d381f184 100644
--- a/include/mysql/plugin_ftparser.h.pp
+++ b/include/mysql/plugin_ftparser.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_function.h.pp b/include/mysql/plugin_function.h.pp
index b70befaf..068a7a2e 100644
--- a/include/mysql/plugin_function.h.pp
+++ b/include/mysql/plugin_function.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_password_validation.h.pp b/include/mysql/plugin_password_validation.h.pp
index 69001306..09a5bb98 100644
--- a/include/mysql/plugin_password_validation.h.pp
+++ b/include/mysql/plugin_password_validation.h.pp
@@ -564,6 +564,22 @@ struct compression_service_snappy_st{
 };
 extern struct compression_service_snappy_st *compression_service_snappy;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_ZSTD;
+typedef size_t (*PTR_ZSTD_compress)( void *dst, size_t dstCapacity, const void *src, size_t srcSize, int compressionLevel );
+typedef size_t (*PTR_ZSTD_compressBound)( size_t srcSize );
+typedef size_t (*PTR_ZSTD_decompress)( void *dst, size_t dstCapacity, const void *src, size_t compressedSize );
+typedef const char* (*PTR_ZSTD_getErrorName)( size_t code );
+typedef unsigned (*PTR_ZSTD_isError)( size_t code );
+struct compression_service_zstd_st{
+    PTR_ZSTD_compress ZSTD_compress_ptr;
+    PTR_ZSTD_compressBound ZSTD_compressBound_ptr;
+    PTR_ZSTD_decompress ZSTD_decompress_ptr;
+    PTR_ZSTD_getErrorName ZSTD_getErrorName_ptr;
+    PTR_ZSTD_isError ZSTD_isError_ptr;
+};
+extern struct compression_service_zstd_st *compression_service_zstd;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/services.h b/include/mysql/services.h
index e39f87bc..008f9da0 100644
--- a/include/mysql/services.h
+++ b/include/mysql/services.h
@@ -48,6 +48,7 @@ extern "C" {
 #include <compression/lzma.h>
 #include <compression/lzo/lzo1x.h>
 #include <compression/snappy-c.h>
+#include <compression/zstd.h>
 
 #ifdef __cplusplus
 }
diff --git a/include/service_versions.h b/include/service_versions.h
index cb3cf1ce..38be6d82 100644
--- a/include/service_versions.h
+++ b/include/service_versions.h
@@ -49,3 +49,4 @@
 #define VERSION_compression_lzma        0x0100
 #define VERSION_compression_lzo         0x0100
 #define VERSION_compression_snappy      0x0100
+#define VERSION_compression_zstd        0x0100
diff --git a/libmysqld/CMakeLists.txt b/libmysqld/CMakeLists.txt
index fbc91b1d..e0eda8ee 100644
--- a/libmysqld/CMakeLists.txt
+++ b/libmysqld/CMakeLists.txt
@@ -143,6 +143,7 @@ SET(SQL_EMBEDDED_SOURCES emb_qcache.cc libmysqld.c lib_sql.cc
            ../libservices/compression_service_lzma.c
            ../libservices/compression_service_lzma.c
            ../libservices/compression_service_snappy.c
+           ../libservices/compression_service_zstd.c
 )
 
 
diff --git a/libservices/CMakeLists.txt b/libservices/CMakeLists.txt
index b3703fc3..56cc0c86 100644
--- a/libservices/CMakeLists.txt
+++ b/libservices/CMakeLists.txt
@@ -43,6 +43,7 @@ SET(MYSQLSERVICES_SOURCES
   compression_service_lzma.c
   compression_service_lzo.c
   compression_service_snappy.c
+  compression_service_zstd.c
   )
 
 ADD_CONVENIENCE_LIBRARY(mysqlservices ${MYSQLSERVICES_SOURCES})
diff --git a/libservices/compression_service_zstd.c b/libservices/compression_service_zstd.c
new file mode 100644
index 00000000..01dac532
--- /dev/null
+++ b/libservices/compression_service_zstd.c
@@ -0,0 +1,14 @@
+/* Copyright (C) 2017 MariaDB Corporation
+   This program is free software; you can redistribute it and/or modify
+   it under the terms of the GNU General Public License as published by
+   the Free Software Foundation; version 2 of the License.
+   This program is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+   GNU General Public License for more details.
+   You should have received a copy of the GNU General Public License
+   along with this program; if not, write to the Free Software
+   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1335  USA */
+
+#include <service_versions.h>
+SERVICE_VERSION compression_service_zstd = (void*) VERSION_compression_zstd;
\ No newline at end of file
diff --git a/sql/CMakeLists.txt b/sql/CMakeLists.txt
index ed3c31eb..09b039c9 100644
--- a/sql/CMakeLists.txt
+++ b/sql/CMakeLists.txt
@@ -170,6 +170,7 @@ SET (SQL_SOURCE
                compression/lzma.cc   ../libservices/compression_service_lzma.c
                compression/lzo.cc    ../libservices/compression_service_lzo.c
                compression/snappy.cc ../libservices/compression_service_snappy.c
+               compression/zstd.cc   ../libservices/compression_service_zstd.c
 )
   
 IF ((CMAKE_SYSTEM_NAME MATCHES "Linux" OR
diff --git a/sql/compression/compression_libs.cc b/sql/compression/compression_libs.cc
index c3b72ed1..90f22164 100644
--- a/sql/compression/compression_libs.cc
+++ b/sql/compression/compression_libs.cc
@@ -6,7 +6,8 @@ void init_compression(
     struct compression_service_bzip2_st  *bzip2_handler,
     struct compression_service_lzma_st   *lzma_handler,
     struct compression_service_lzo_st    *lzo_handler,
-    struct compression_service_snappy_st *snappy_handler
+    struct compression_service_snappy_st *snappy_handler,
+    struct compression_service_zstd_st   *zstd_handler
 ){
     if(enabled_compression_libraries & COMPRESSION_ALL)
         enabled_compression_libraries = (COMPRESSION_ALL) - 1;
@@ -30,4 +31,5 @@ void init_compression(
     init_lzma   (lzma_handler,   (enabled_compression_libraries & COMPRESSION_LZMA));
     init_lzo    (lzo_handler,    (enabled_compression_libraries & COMPRESSION_LZO));
     init_snappy (snappy_handler, (enabled_compression_libraries & COMPRESSION_SNAPPY));
+    init_zstd   (zstd_handler,   (enabled_compression_libraries & COMPRESSION_ZSTD));
 }
diff --git a/sql/compression/compression_libs.h b/sql/compression/compression_libs.h
index 48086724..1633ff60 100644
--- a/sql/compression/compression_libs.h
+++ b/sql/compression/compression_libs.h
@@ -2,6 +2,7 @@
 #include <compression/lzma.h>
 #include <compression/lzo/lzo1x.h>
 #include <compression/snappy-c.h>
+#include <compression/zstd.h>
 
 #define COMPRESSION_BZIP2   1 << 0
 #define COMPRESSION_LZ4     1 << 1
@@ -17,10 +18,12 @@ void init_compression(
     struct compression_service_bzip2_st  *,
     struct compression_service_lzma_st   *,
     struct compression_service_lzo_st    *,
-    struct compression_service_snappy_st *
+    struct compression_service_snappy_st *,
+    struct compression_service_zstd_st   *
 );
 
 void init_bzip2  (struct compression_service_bzip2_st  *, bool);
 void init_lzma   (struct compression_service_lzma_st   *, bool);
 void init_lzo    (struct compression_service_lzo_st    *, bool);
 void init_snappy (struct compression_service_snappy_st *, bool);
+void init_zstd   (struct compression_service_zstd_st   *, bool);
diff --git a/sql/compression/zstd.cc b/sql/compression/zstd.cc
new file mode 100644
index 00000000..8a11edce
--- /dev/null
+++ b/sql/compression/zstd.cc
@@ -0,0 +1,71 @@
+#include "compression_libs.h"
+#include <dlfcn.h>
+
+bool COMPRESSION_LOADED_ZSTD = false;
+
+// Most functions return the (un)compressed size, not an error code
+
+DEFINE_ZSTD_compress(DUMMY_ZSTD_compress){
+    return 0;
+}
+
+DEFINE_ZSTD_compressBound(DUMMY_ZSTD_compressBound){
+    return 0;
+}
+
+DEFINE_ZSTD_decompress(DUMMY_ZSTD_decompress){
+    return 0;
+}
+
+DEFINE_ZSTD_getErrorName(DUMMY_ZSTD_getErrorName){
+    return "ZStd is not loaded.";
+}
+
+DEFINE_ZSTD_isError(DUMMY_ZSTD_isError){
+    return 1;
+}
+
+
+void init_zstd(struct compression_service_zstd_st *handler, bool link_library){
+    //point struct to right place for static plugins
+    compression_service_zstd = handler;
+
+    compression_service_zstd->ZSTD_compress_ptr      = DUMMY_ZSTD_compress;
+    compression_service_zstd->ZSTD_compressBound_ptr = DUMMY_ZSTD_compressBound;
+    compression_service_zstd->ZSTD_decompress_ptr    = DUMMY_ZSTD_decompress;
+    compression_service_zstd->ZSTD_getErrorName_ptr  = DUMMY_ZSTD_getErrorName;
+    compression_service_zstd->ZSTD_isError_ptr       = DUMMY_ZSTD_isError;
+
+    if(!link_library)
+        return;
+
+    //Load ZStd library dynamically
+    void *library_handle = dlopen("libzstd.so", RTLD_LAZY | RTLD_GLOBAL);
+    if(!library_handle || dlerror()){
+        //sql_print_warning("Could not open libzstd.so\n");
+        return;
+    }
+
+    void *ZSTD_compress_ptr      = dlsym(library_handle, "ZSTD_compress");
+    void *ZSTD_compressBound_ptr = dlsym(library_handle, "ZSTD_compressBound");
+    void *ZSTD_decompress_ptr    = dlsym(library_handle, "ZSTD_decompress");
+    void *ZSTD_getErrorName_ptr  = dlsym(library_handle, "ZSTD_getErrorName");
+    void *ZSTD_isError_ptr       = dlsym(library_handle, "ZSTD_isError");
+    
+    if(
+        !ZSTD_compress_ptr      ||
+        !ZSTD_compressBound_ptr ||
+        !ZSTD_decompress_ptr    ||
+        !ZSTD_getErrorName_ptr  ||
+        !ZSTD_isError_ptr
+    )
+        return;
+    
+    compression_service_zstd->ZSTD_compress_ptr      = (PTR_ZSTD_compress)      ZSTD_compress_ptr;
+    compression_service_zstd->ZSTD_compressBound_ptr = (PTR_ZSTD_compressBound) ZSTD_compressBound_ptr;
+    compression_service_zstd->ZSTD_decompress_ptr    = (PTR_ZSTD_decompress)    ZSTD_decompress_ptr;
+    compression_service_zstd->ZSTD_getErrorName_ptr  = (PTR_ZSTD_getErrorName)  ZSTD_getErrorName_ptr;
+    compression_service_zstd->ZSTD_isError_ptr       = (PTR_ZSTD_isError)       ZSTD_isError_ptr;
+    
+    COMPRESSION_LOADED_ZSTD = true;
+} 
\ No newline at end of file
diff --git a/sql/mysqld.cc b/sql/mysqld.cc
index 5aaf9bfc..b44fab39 100644
--- a/sql/mysqld.cc
+++ b/sql/mysqld.cc
@@ -7322,6 +7322,7 @@ SHOW_VAR status_vars[]= {
   {"Compression_loaded_lzma",  (char*) &COMPRESSION_LOADED_LZMA,   SHOW_BOOL},
   {"Compression_loaded_lzo",   (char*) &COMPRESSION_LOADED_LZO,    SHOW_BOOL},
   {"Compression_loaded_snappy",(char*) &COMPRESSION_LOADED_SNAPPY, SHOW_BOOL},
+  {"Compression_loaded_zstd",  (char*) &COMPRESSION_LOADED_ZSTD,   SHOW_BOOL},
   {"Connections",              (char*) &global_thread_id,         SHOW_LONG_NOFLUSH},
   {"Connection_errors_accept", (char*) &connection_errors_accept, SHOW_LONG},
   {"Connection_errors_internal", (char*) &connection_errors_internal, SHOW_LONG},
diff --git a/sql/sql_plugin.cc b/sql/sql_plugin.cc
index 73be5b4f..07c99cde 100644
--- a/sql/sql_plugin.cc
+++ b/sql/sql_plugin.cc
@@ -1641,7 +1641,8 @@ int plugin_init(int *argc, char **argv, int flags)
     &compression_handler_bzip2,
     &compression_handler_lzma,
     &compression_handler_lzo,
-    &compression_handler_snappy
+    &compression_handler_snappy,
+    &compression_handler_zstd
   );
 
   /* prepare encryption_keys service */
diff --git a/sql/sql_plugin_services.ic b/sql/sql_plugin_services.ic
index de53e3be..ee5cac99 100644
--- a/sql/sql_plugin_services.ic
+++ b/sql/sql_plugin_services.ic
@@ -233,6 +233,7 @@ static struct compression_service_bzip2_st  compression_handler_bzip2  = {};
 static struct compression_service_lzma_st   compression_handler_lzma   = {};
 static struct compression_service_lzo_st    compression_handler_lzo    = {};
 static struct compression_service_snappy_st compression_handler_snappy = {};
+static struct compression_service_zstd_st   compression_handler_zstd   = {};
 
 static struct st_service_ref list_of_services[]=
 {
@@ -263,5 +264,6 @@ static struct st_service_ref list_of_services[]=
   { "compression_service_bzip2",   VERSION_compression_bzip2,   &compression_handler_bzip2 },
   { "compression_service_lzma",    VERSION_compression_lzma,    &compression_handler_lzma },
   { "compression_service_lzo",     VERSION_compression_lzo,     &compression_handler_lzo },
-  { "compression_service_snappy",  VERSION_compression_snappy,  &compression_handler_snappy }
+  { "compression_service_snappy",  VERSION_compression_snappy,  &compression_handler_snappy },
+  { "compression_service_zstd",    VERSION_compression_zstd,    &compression_handler_zstd }
 };
-- 
2.26.2.windows.1

